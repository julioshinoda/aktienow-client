import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';
import '../imports/api/avaliacao.js';


Meteor.startup(() => {
  // code to run on server at startup
});


var usuarioValidaCall = function (usuarioNome, callback) {
  // try…catch allows you to handle errors 
  try {
    // var response = HTTP.get(apiUrl).data;
    // // A successful API call returns no error 
    // // but the contents from the JSON response
    // callback(null, response);


    var response =  HTTP.call("GET", "http://localhost:3002/usuario/nome", {params:{usuario:usuarioNome}});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}

var usuarioAdminValidaCall = function (usuario, callback) {
  // try…catch allows you to handle errors 
  try {
    // var response = HTTP.get(apiUrl).data;
    // // A successful API call returns no error 
    // // but the contents from the JSON response
    // callback(null, response);


    var response =  HTTP.call("GET", "http://localhost:3002/usuario/admin", {params:{"usuario":usuario.usuario,"senha":usuario.senha}});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}

var AvaliacaoLivrosCall = function (usuarioId, callback) {
  // try…catch allows you to handle errors 
  try {
    // var response = HTTP.get(apiUrl).data;
    // // A successful API call returns no error 
    // // but the contents from the JSON response
    // callback(null, response);


    var response =  HTTP.call("GET", "http://localhost:3002/avaliacao/livros", {params:{usuario_id:usuarioId}});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}

var AvaliacaoListaCompletaCall = function ( callback) {

  try {
   var response =  HTTP.call("GET", "http://localhost:3002/avaliacao/lista", {});
   callback(null, response);

 } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}

var AvaliarLivroCall = function (avaliacao, callback) {
  // try…catch allows you to handle errors 
  try {
    // var response = HTTP.get(apiUrl).data;
    // // A successful API call returns no error 
    // // but the contents from the JSON response
    // callback(null, response);


    var response =  HTTP.call("POST", "http://localhost:3002/avaliacao", {data:avaliacao, headers: {
      'Content-Type': 'application/json'
    }});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}

var CadastrarUsuarioCall = function (usuario, callback) {

  try {
    var response =  HTTP.call("POST", "http://localhost:3002/usuario", {data:usuario, headers: {
      'Content-Type': 'application/json'
    }});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}

var CadastrarLivroCall = function (livro, callback) {

  try {
    var response =  HTTP.call("POST", "http://localhost:3002/livro", {data:livro, headers: {
      'Content-Type': 'application/json'
    }});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}

var UsuariosCall = function ( callback) {

  try {
    var response =  HTTP.call("GET", "http://localhost:3002/usuario/lista", {});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}

var UsuarioLivrosCall = function (usuario ,callback) {

  try {
    var response =  HTTP.call("GET", "http://localhost:3002/usuario/livros", {params:{usuario_id:usuario.id}});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}


var UsuarioMediaCall = function (usuario ,callback) {

  try {
    var response =  HTTP.call("GET", "http://localhost:3002/usuario/media", {params:{usuario_id:usuario.id}});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}


var LivrosCall = function ( callback) {

  try {
    var response =  HTTP.call("GET", "http://localhost:3002/livro/lista", {});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}

var LivroDadosCall = function (livro ,callback) {

  try {
    var response =  HTTP.call("GET", "http://localhost:3002/livro/dados", {params:{livro_id:livro.id}});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}


var LivroMediaCall = function (livro ,callback) {

  try {
    var response =  HTTP.call("GET", "http://localhost:3002/livro/media", {params:{livro_id:livro.id}});
    callback(null, response);

  } catch (error) {
    // If the API responded with an error message and a payload 
    if (error.response) {
      var errorCode = error.response.data.code;
      var errorMessage = error.response.data.message;
      // Otherwise use a generic error message
    } else {
      var errorCode = 500;
      var errorMessage = 'Cannot access the API';
    }
    // Create an Error object and return it via callback
    var myError = new Meteor.Error(errorCode, errorMessage);
    callback(myError, null);
  }
}


Meteor.methods({
  'VerificaUsuario': function (usuario) {
    this.unblock();
    // asynchronous call to the dedicated API calling function
    var response = Meteor.wrapAsync(usuarioValidaCall)(usuario);

    console.log(response);
    return response;
  },'VerificaUsuarioAdmin': function (usuario) {
    this.unblock();
    // asynchronous call to the dedicated API calling function
    var response = Meteor.wrapAsync(usuarioAdminValidaCall)(usuario);

    console.log(response);
    return response;
  },
  'AvaliacaoLivros': function (usuario) {
    this.unblock();
    // asynchronous call to the dedicated API calling function
    var response = Meteor.wrapAsync(AvaliacaoLivrosCall)(usuario);

    console.log(response);
    return response;
  },
  'AvaliacaoListaCompleta': function () {
    this.unblock();
    // asynchronous call to the dedicated API calling function
    var response = Meteor.wrapAsync(AvaliacaoListaCompletaCall)();

    console.log(response);
    return response;
  },
  'AvaliarLivro': function (avaliacao) {
    this.unblock();
    // asynchronous call to the dedicated API calling function
    var response = Meteor.wrapAsync(AvaliarLivroCall)(avaliacao);

    console.log(response);
    return response;
  },
  'CadastrarUsuario': function (usuario) {
    this.unblock();
    // asynchronous call to the dedicated API calling function
    var response = Meteor.wrapAsync(CadastrarUsuarioCall)(usuario);

    console.log(response);
    return response;
  },
  'CadastrarLivro': function (livro) {
    this.unblock();
    // asynchronous call to the dedicated API calling function
    var response = Meteor.wrapAsync(CadastrarLivroCall)(livro);

    console.log(response);
    return response;
  },
  'RemoveAll': function () {
    this.unblock();
    // asynchronous call to the dedicated API calling function
     return  Avaliacoes.remove();
  },
  'UsuariosLivros': function () {
    this.unblock();
    var usuarios = Meteor.wrapAsync(UsuariosCall)();
   

    var response = usuarios.data.map(function(elem, index){
       console.log('teste ,');
      console.log(elem);
      var livros  = Meteor.wrapAsync(UsuarioLivrosCall)(elem);
      var media  = Meteor.wrapAsync(UsuarioMediaCall)(elem);
      elem.livros = livros.data ;
      elem.media = media.data[0].media ;
      return elem;
    });

    return response;
  },
  'LivrosDados': function () {
    this.unblock();
    var livros = Meteor.wrapAsync(LivrosCall)();
   

    var response = livros.data.map(function(elem, index){
                          var dados  = Meteor.wrapAsync(LivroDadosCall)(elem);
                          var media  = Meteor.wrapAsync(LivroMediaCall)(elem);
                          elem.dados = dados.data ;

                          var total = dados.data.reduce(function(prev, elem) {
                            return prev + elem.qtd_estado;
                          },0);

                          elem.total = total;

                          var porcentagem = dados.data.map(function(elem2, index){
                                                 elem2.porcentagem = (elem2.qtd_estado/elem.total) *100;
                                                 return elem2;
                                              });

                          elem.porcentagem = porcentagem;  
                      
                                            

                          elem.media = media.data[0].media ;
                          return elem;
                        });

    return response;
  }
});
