FlowRouter.route( '/', {
  action: function() {
                BlazeLayout.render( 'principal', {top: "header", main: 'usuario' } ); 
},
  name: 'principal'
});

FlowRouter.route( '/form-avaliacao', {
  action: function(params, queryParams) {
  	console.log("Query Params:"+queryParams.id);
                BlazeLayout.render( 'principal', {top: "header", main: 'main_form_avaliacao' } ); 
},
  name: 'principal'
});

FlowRouter.route( '/admin', {
  action: function() {
                BlazeLayout.render( 'principal', {top: "header_admin", main: 'main_admin' } ); 
},
  name: 'principal'
});
