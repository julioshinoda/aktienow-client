import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { HTTP } from 'meteor/http';
import { Avaliacoes } from '../../imports/api/avaliacao.js';


import './../main.html';

// import datatables from 'datatables.net';
// import datatables_bs from 'datatables.net-bs';
 import 'datatables.net-bs/css/dataTables.bootstrap.css';

Template.main_admin.onCreated(function () {
  this.adminState = new ReactiveDict();

  const instance = Template.instance();
  instance.adminState.set('logado',false);

  // datatables(window, $);
  // datatables_bs(window, $);

  Meteor.call('AvaliacaoListaCompleta', {}, function (err, res) {
    
      if (err) {

        instance.adminState.set('resposta', { error: err });
      } else {
        var lista =  JSON.parse( res.content );
        instance.adminState.set('listaCompleta', JSON.parse( res.content ));
        Meteor.call('AvaliacaoListaCompleta', {}, function (err, res) {


        dataTableData = function () {
            return JSON.parse( res.content ); // or .map()
        };
        });

      }
    });

  Meteor.call('UsuariosLivros',{},function(err, res){
      if (err) {
        console.log(err);
        instance.adminState.set('resposta', { error: err });
      } else {
         // var lista =  JSON.parse( res.content );
         instance.adminState.set('usuariosLivros', res);
     

      }
  });


  Meteor.call('LivrosDados',{},function(err, res){
      if (err) {
        console.log(err);
        instance.adminState.set('resposta', { error: err });
      } else {
         // var lista =  JSON.parse( res.content );
         instance.adminState.set('livrosDados', res);
        console.log(res);

      }
  });
  
});
var optionsObject = {
    columns: [{
        title: 'Livro',
        data: 'nome', // note: access nested data like this
        className: 'nameColumn'
    }, {
        title: 'Estado',
        data: 'estado',
        className: 'imageColumn'
    }, {
        title: 'Nota',
        data: 'nota',
        className: 'imageColumn'
    }, {
        title: 'Observação',
        data: 'observacao',
        className: 'imageColumn'
    }],
    // ... see jquery.dataTables docs for more
}


Template.main_admin.helpers({
 resposta: function () {
  const instance = Template.instance();
  return instance.adminState.get('resposta');
},
logado: function(){
  const instance = Template.instance();
  return instance.adminState.get('logado');
},
lista: function () {
  const instance = Template.instance();
  return instance.adminState.get('listaCompleta');
},
usuariosLivros: function () {
  const instance = Template.instance();
  return instance.adminState.get('usuariosLivros');
},
livrosDados: function () {
  const instance = Template.instance();
  return instance.adminState.get('livrosDados');
},
reactiveDataFunction: function () {
        return dataTableData;
    },
optionsObject: optionsObject
});



Template.main_admin.events({
'click .login-admin': function (evt, instance) {
  var usuario = new Object();
  usuario.usuario =instance.find('input#usuario').value;
  usuario.senha = instance.find('input#senha').value;

  Meteor.call('VerificaUsuarioAdmin', usuario, function (err, res) {
     // console.log(JSON.parse( res.content )[0].id);
      // The method call sets the Session variable to the callback value
      if (err) {

        instance.adminState.set('resposta', { error: err });
      } else {
        if(JSON.parse( res.content ).length == 0){
          instance.adminState.set('resposta',true);
          return res;
        }else{
          instance.adminState.set('logado',true);
          //FlowRouter.go("/form-avaliacao?id="+JSON.parse( res.content )[0].id);
        }
        

        // instance.stateEndereco.set('resposta', res);
        // return res;
      }
    });
},
'click .cadastrar-usuario': function (evt, instance) {
  var usuario = new Object();
  usuario.usuario = instance.find('#usuario').value;

  Meteor.call('CadastrarUsuario', usuario, function (err, res) {
          //console.log(JSON.parse( res.content )[0].id);
          // The method call sets the Session variable to the callback value
          if (err) {

            instance.adminState.set('resposta', { error: err });
          } else {
            instance.find('#usuario').value = '';

            Modal.show('main_admin');

          }
        });
},
'click .cadastrar-livro': function (evt, instance) {
  var livro = new Object();
  livro.nome = instance.find('#livro').value;

  Meteor.call('CadastrarLivro', livro, function (err, res) {
          //console.log(JSON.parse( res.content )[0].id);
          // The method call sets the Session variable to the callback value
          if (err) {

            instance.adminState.set('resposta', { error: err });
          } else {
            instance.find('#livro').value = '';

            Modal.show('main_admin');

          }
        });
}
});