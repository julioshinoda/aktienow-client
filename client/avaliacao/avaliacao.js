import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { HTTP } from 'meteor/http';
import './../main.html';


Template.main_form_avaliacao.onCreated(function () {
	 this.avaliacaoState = new ReactiveDict();

    var usuario_id = FlowRouter.getQueryParam("id");
    const instance = Template.instance();
    Meteor.call('AvaliacaoLivros', usuario_id, function (err, res) {

      // The method call sets the Session variable to the callback value
      if (err) {

        instance.avaliacaoState.set('livros', { error: err });
      } else {
        if(JSON.parse( res.content ).length == 0){
          instance.avaliacaoState.set('livros',false);
           return res;
        }else{
          instance.avaliacaoState.set('temLivros',true);
          instance.avaliacaoState.set('livros',JSON.parse( res.content ));
        }
      
      }
    });

});

Template.main_form_avaliacao.helpers({
   resposta: function () {
    const instance = Template.instance();
    return instance.avaliacaoState.get('resposta');
  },
   livros: function () {
    const instance = Template.instance();
    return instance.avaliacaoState.get('livros');
  },
  temLivros: function(){
    const instance = Template.instance();
    return instance.avaliacaoState.get('temLivros');
  }
});



Template.main_form_avaliacao.events({
   'click .avaliar': function (evt, instance) {
    var avaliacao = new Object();
    avaliacao.livro_id = instance.find('#livro_id').value;
    avaliacao.estado = instance.find('#estado').value;
    avaliacao.nota = instance.find('#nota').value;
    avaliacao.observacao = instance.find('#observacao').value;
    avaliacao.usuario_id = FlowRouter.getQueryParam("id");

    Meteor.call('AvaliarLivro', avaliacao, function (err, res) {
      //console.log(JSON.parse( res.content )[0].id);
      // The method call sets the Session variable to the callback value
      if (err) {

        instance.avaliacaoState.set('resposta', { error: err });
      } else {
     
           Meteor.call('AvaliacaoLivros', FlowRouter.getQueryParam("id"), function (err, res) {


                if (err) {

                  instance.avaliacaoState.set('livros', { error: err });
                } else {
                  if(JSON.parse( res.content ).length == 0){
                    instance.avaliacaoState.set('livros',false);
                    instance.avaliacaoState.set('temLivros',false);
                  }else{
                    console.log('atualiza : '+JSON.parse( res.content )  );
                    instance.avaliacaoState.set('livros',JSON.parse( res.content ));
                    instance.avaliacaoState.set('temLivros',true);

                  }
                
                }
            });

        console.log('sucesso');
         Modal.show('main_form_avaliacao');

      }
    });
  },
  'click .return-home': function (evt, instance) {
        FlowRouter.go("/");
  },
  'click .nova-avaliacao': function (evt, instance) {
        
  }
});
