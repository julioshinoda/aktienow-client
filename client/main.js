import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { HTTP } from 'meteor/http';

import './main.html';


Template.usuario.onCreated(function () {
	 this.usuarioState = new ReactiveDict();
});

Template.usuario.helpers({
   resposta: function () {
    const instance = Template.instance();
    return instance.usuarioState.get('resposta');
  }
});



Template.usuario.events({
   'click button': function (evt, instance) {
    var usuarioNome = instance.find('input#usuario').value;
   

      Meteor.call('VerificaUsuario', usuarioNome, function (err, res) {
     // console.log(JSON.parse( res.content )[0].id);
      // The method call sets the Session variable to the callback value
      if (err) {

        instance.usuarioState.set('resposta', { error: err });
      } else {
        if(JSON.parse( res.content ).length == 0){
          instance.usuarioState.set('resposta',true);
           return res;
        }else{
          FlowRouter.go("/form-avaliacao?id="+JSON.parse( res.content )[0].id);
        }
      

        // instance.stateEndereco.set('resposta', res);
        // return res;
      }
    });


  },
  'click #usuario':function(evt, instance){
    if(instance.usuarioState.get('resposta')){
      instance.usuarioState.set('resposta',false);
    }
  }
});
